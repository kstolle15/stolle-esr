# Stolle Employee Reimbursement System
#### Project Description
Stolle Employee Reimbursement System is a full stack web application that keeps track of employee information as well as each employee's 
reimbursement request history. It utilizes Java, Servlets, and an Oracle Database, hosted on AWS, for the back end of the application.
On the front end, HTML/CSS, JavaScript, jQuery, AJAX, and Bootstrap are used to created the GUI. Other technologies used in this application were
Postman, to test Servlet responses to HTTP requests, and Tomcat, to serve as a container for the Servlets.

#### Technologies Used 
- Java - 8
- Oracle DB - 19.8.0.0
- JUnit - 4.12
- javax.servlet - 2.5
- Tomcat - 9.0.41
- HTML - 5 
- CSS - 3
- Bootstrap - 5
- JavaScript - ES6
- Postman - Version 8.0.3
- log4j - 1.2.17
- Spring Tool Suite - 4
#### Features
###### Features completed
- Employee can login
- Employee can view their own information 
- Employee can view pending and resolved reimbursement requests
- Employee can submit a new reimbursement request
- Manager can login 
- Manager can view their own information 
- Manager can view all pending reimbursement requests 
- Manager can approve or reject any pending reimbursement request 

###### Features To be completed
- An Employee can update their information 
- A Manager can view all resolved requests from all employees and see which manager resolved it
- A Manager can view all Employees
- An Employee can upload an image of his/her receipt as part of the reimbursement request
- A Manager can view images of the receipts from reimbursement requests
- An Employee receives an email when one of their reimbursement requests is resolved
- A Manager can register an Employee, which sends the Employee an email with their username and temp password
- An Employee can reset their password
- Password encryption
- Including visual aids, such as charts showing spending categories or spending over time
- Ensuring code quality with a static code analysis 

#### Getting Started 
1. Clone Repository using git bash(windows) or the terminal(LINUX): `git clone https://gitlab.com/kstolle15/stolle-esr.git` 
2. Open project in Spring Tool Suite 4
3. Open Servers view and click on "create new server"
4. Specify the tomcat environment you want to run, then select the project you want to link this server with and click finish.
5. Start the server by right clicking on it and selecting Start
6. Finally open login.html in prefered web browser to interact with the web pages.

#### Usage
Once you login to the system, a new web page should load based on what type of user you are (Manager or Employee).
NOTE. Currently there is no way to create new employees. That feature is currently under the to be completed category.

#### Contributors
Kyle Stolle - @kstolle15
