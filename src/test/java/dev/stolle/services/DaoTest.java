package dev.stolle.services;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dev.stolle.daos.RequestDao;
import dev.stolle.daos.RequestDaoImpl;
import dev.stolle.daos.UserDao;
import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.Request;
import dev.stolle.models.User;

public class DaoTest {
	private UserDao testUserDao = new UserDaoImpl();
	private RequestDao testRequestDao = new RequestDaoImpl();
	
	@Test
	public void testNullGetUser() {
		User expected = null;
		User actual = testUserDao.getUser(null, null);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInvalidGetUser() {
		User expected = null;
		User actual = testUserDao.getUser("","");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInvalidGetUserById() {
		User expected = null;
		User actual = testUserDao.getUser(-1);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testNullGetAllUserRequests() {
		List<Request> expected = new ArrayList<Request>();
		List<Request> actual = testUserDao.getAllUserRequests(null);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInvalidGetAllUserRequests() {
		List<Request> expected = new ArrayList<Request>();
		List<Request> actual = testUserDao.getAllUserRequests(new User());
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testNullGetPendingRequests() {
		List<Request> expected = new ArrayList<Request>();
		List<Request> actual = testUserDao.getPendingRequests(null);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testInvalidGetPendingRequests() {
		List<Request> expected = new ArrayList<Request>();
		List<Request> actual = testUserDao.getPendingRequests(new User());
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testNullGetResolvedRequests() {
		List<Request> expected = new ArrayList<Request>();
		List<Request> actual = testUserDao.getResolvedRequests(null);
		assertEquals(expected, actual);
	}
	
	@Test 
	public void testInvalidGetResolvedRequests() {
		List<Request> expected = new ArrayList<Request>();
		User user = new User();
		List<Request> actual = testUserDao.getResolvedRequests(user);
		assertEquals(expected, actual);
	} 
	
	@Test (expected = NullPointerException.class)
	public void testNullUpdateUserReimbursement() throws SQLException {
		testUserDao.updateUserReimbursement(null, 0);
	}
	
	@Test (expected = NullPointerException.class)
	public void testNullApproveRequest() throws SQLException {
		testRequestDao.approveRequest(null);
	} 
	
	@Test 
	public void testInvalidApproveRequest() throws SQLException {
		boolean expected = false;
		boolean actual = testRequestDao.approveRequest(new Request());
		assertEquals(expected, actual);
	}
	
	@Test (expected = NullPointerException.class)
	public void testNullRejectRequest() throws SQLException {
		testRequestDao.rejectRequest(null);
	}
	
	@Test 
	public void testInvalidRejectRequest() throws SQLException {
		boolean expected = false;
		boolean actual = testRequestDao.rejectRequest(new Request());
		assertEquals(expected, actual);
	}
	
	@Test (expected = NullPointerException.class)
	public void testNullSubmitRequest() throws SQLException {
		testRequestDao.submitRequest(null);
	}
}
