package dev.stolle.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import dev.stolle.models.Request;
import dev.stolle.models.RequestStatus;
import dev.stolle.models.User;
import dev.stolle.models.UserType;
import dev.stolle.util.ConnectionUtil;

public class UserDaoImpl implements UserDao {
	private static Logger log = Logger.getRootLogger();
	
	public User getUser(String userName, String password) {
		log.info("Getting User: " + userName + " information.");
		String sql = "SELECT * FROM Users WHERE User_Name = ? AND Password = ?";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setString(1, userName);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next() == false) {
				log.error("User: " + userName + " does not exist in the database.");
				return null;
			} 
			User user = new User();
			user.setUserId(rs.getInt("User_Id"));
			user.setUserName(userName);
			user.setPassword(password);
			user.setFirstName(rs.getString("First_Name"));
			user.setLastName(rs.getString("Last_Name"));
			user.setBirthday(rs.getString("Birthday"));
			user.setTotalReimbursementAmount(rs.getDouble("Total_Reimbursement_Amount"));
			String type = rs.getString("User_Type");
			if (type.equals("E")) {
				 user.setUserType(UserType.EMPLOYEE);
			} else {
				user.setUserType(UserType.MANAGER);
			}
			user.setRequests(getAllUserRequests(user));
			return user;
			
		} catch (SQLException ex) {
			log.error("Error retrieving: " + userName + " from database");
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public User getUser(int userId) {
		log.info("Getting User: " + userId + " information.");
		String sql = "SELECT * FROM Users WHERE User_Id = ?";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			if(rs.next() == false) {
				log.error("User: " + userId + " does not exist in the database.");
				return null;
			} 
			User user = new User();
			user.setUserId(userId);
			user.setUserName(rs.getString("User_Name"));
			user.setPassword(rs.getString("Password"));
			user.setFirstName(rs.getString("First_Name"));
			user.setLastName(rs.getString("Last_Name"));
			user.setBirthday(rs.getString("Birthday"));
			user.setTotalReimbursementAmount(rs.getDouble("Total_Reimbursement_Amount"));
			String type = rs.getString("User_Type");
			if (type.equals("E")) {
				 user.setUserType(UserType.EMPLOYEE);
			} else {
				user.setUserType(UserType.MANAGER);
			}
			user.setRequests(getAllUserRequests(user));
			return user;
		} catch (SQLException ex) {
			log.error("Error retrieving user by id.");
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public List<Request> getAllUserRequests(User user){
		List<Request> requests = new ArrayList<Request>();
		if (user == null) {
			return requests;
		}
		log.info("Retrieving the requests for user: " + user.getUserName());
		String sql = "SELECT * FROM Requests WHERE User_Id = ? ORDER BY Request_Id";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setInt(1, user.getUserId());
			ResultSet rs = ps.executeQuery();

			
			while(rs.next()) {
				Request request = new Request();
				request.setRequestId(rs.getInt("Request_Id"));
				request.setUserId(user.getUserId());
				request.setAmount(rs.getDouble("Amount"));
				String status = rs.getString("Status");
				if(status.equals("PENDING")) {
					request.setStatus(RequestStatus.PENDING);
				} else if (status.equals("APPROVED")) {
					request.setStatus(RequestStatus.APPROVED);
				} else {
					request.setStatus(RequestStatus.REJECTED);
				}
				requests.add(request);
			}
			if(requests.size() > 0) {
				log.info("Successfully retrieved user: " + user.getUserName() + " requests.");
			} else {
				log.info("User: " + user.getUserName() + " had no requests");
			}
		} catch (SQLException ex) {
			log.error("Error retrieving requests. SQLException caught.");
			ex.printStackTrace();
		}
		return requests;
	}
	
	public List<Request> getPendingRequests(User user){
		List<Request> requests = new ArrayList<Request>();
		if(user == null) {
			return requests;
		}
		log.info("Attempting to retrieve user: " + user.getUserName() + " pending requests.");
		String sql = "SELECT * FROM Requests WHERE User_Id = ? AND Status = ? ORDER BY Request_Id";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setInt(1, user.getUserId());
			ps.setString(2, "PENDING");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Request request = new Request();
				request.setRequestId(rs.getInt("Request_Id"));
				request.setUserId(user.getUserId());
				request.setAmount(rs.getDouble("Amount"));
				String status = rs.getString("Status");
				if(status.equals("PENDING")) {
					request.setStatus(RequestStatus.PENDING);
				} else {
					log.error("Error retrieving non pending requests");
					return null;
				} 
				requests.add(request);
				
			}
			if(requests.size() > 0) {
				log.info("Successfully retrieved user: " + user.getUserName() + " pending requests.");
			} else {
				log.info("User: " + user.getUserName() + " had no pending requests");
			}
		} catch (SQLException ex) {
			log.error("Error retrieving pending requests for user: " + user.getUserName() + ". SQLException thrown");
			ex.printStackTrace();
		}
		return requests;
	}
	
	public List<Request> getResolvedRequests(User user){
		List<Request> requests = new ArrayList<Request>();
		if(user == null) {
			return requests;
		}
		log.info("Attempting to retrieve all resolved requests for user: " + user.getUserName());
		String sql = "SELECT * FROM Requests WHERE User_Id = ? AND (Status = ? OR Status = ?) ORDER BY Request_Id";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setInt(1, user.getUserId());
			ps.setString(2, "APPROVED");
			ps.setString(3, "REJECTED");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Request request = new Request();
				request.setRequestId(rs.getInt("Request_Id"));
				request.setUserId(user.getUserId());
				request.setAmount(rs.getDouble("Amount"));
				String status = rs.getString("Status");
				if(status.equals("APPROVED")) {
					request.setStatus(RequestStatus.APPROVED);
				} else if (status.equals("REJECTED")) {
					request.setStatus(RequestStatus.REJECTED);
				} else {
					log.error("Error. Pending requests were returned when trying to get resolved requests.");
					return null;
				}
				requests.add(request);
			}
			if(requests.size() > 0) {
				log.info("Successfully retrieved user: " + user.getUserName() + " resolved requests.");
			} else {
				log.info("User: " + user.getUserName() + " had no resolved requests");
			}
			
		} catch (SQLException ex) {
			log.error("Error retrieving all resolved records for user: " + user.getUserName() + ". SQLException thrown.");
			ex.printStackTrace();
		}
		return requests;
	}
	
	public List<Request> getAllPendingRequests(){
		List<Request> requests = new ArrayList<Request>();
		log.info("Attempting to retrieve all requests in table");
		String sql = "SELECT * FROM Requests WHERE Status = ? ORDER BY Request_Id";
		try(Connection conn = ConnectionUtil.getConnection(); PreparedStatement ps = conn.prepareStatement(sql);){
			ps.setString(1, "PENDING");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Request request = new Request(); 
				request.setRequestId(rs.getInt("Request_Id"));
				request.setUserId(rs.getInt("User_Id"));
				request.setAmount(rs.getDouble("Amount"));
				String status = rs.getString("Status");
				if(status.equals("PENDING")) {
					request.setStatus(RequestStatus.PENDING);
				} else {
					log.error("Error. Query returning non-pending requests");
				}
				requests.add(request);
			}
			if(requests.size() > 0) {
				log.info("Successfully retrieved all pending requests.");
			} else {
				log.info("No pending requests on the database");
			}
		} catch (SQLException ex) {
			log.error("Error retrieving all requests. SQLException caught.");
			ex.printStackTrace();
		}
		return requests;
	} 
	
	public boolean updateUserReimbursement(User user, double amount) throws SQLException {
		log.info("Updating user reimbursement amount: " + user.getUserId());
		String sql = "UPDATE Users SET Total_Reimbursement_Amount = ? WHERE User_Id = ?";
		Connection conn = null;
		boolean update = false;
		try {
			conn = ConnectionUtil.getConnection();
			try (PreparedStatement ps = conn.prepareStatement(sql);) {
				double newAmount = user.getTotalReimbursementAmount() + amount;
				ps.setDouble(1, newAmount);
				ps.setInt(2, user.getUserId());
				int updateRows = ps.executeUpdate();
				if(updateRows == 1) {
					log.info("Successfully updated user total reimbursement");
					update = true;
				} 
			} 
		} catch (SQLException ex) {
			log.error("Error updating user total reimbursement amount.");
			ex.printStackTrace();
		} finally {
			if(conn != null) {
				conn.close();
			}
		}
		return update;
	}
}
