package dev.stolle.daos;

import java.sql.SQLException;

import dev.stolle.models.Request;
import dev.stolle.models.User;

public interface RequestDao {
	public boolean approveRequest(Request  request) throws SQLException;
	public boolean rejectRequest(Request request) throws SQLException;
	public boolean submitRequest(Request request) throws SQLException;
}
