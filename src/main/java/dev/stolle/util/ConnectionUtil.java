package dev.stolle.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dev.stolle.models.User;

public class ConnectionUtil {
	
private static Connection connection;
	
	public static Connection getConnection() throws SQLException {
		String url = System.getenv("AWS_URL");
		String username = System.getenv("AWS_USER");
		String password = System.getenv("AWS_PASS");
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			if (connection == null || connection.isClosed()) {
				connection = DriverManager.getConnection(url,username,password);
			}
			
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		
		return connection;
	}

}
