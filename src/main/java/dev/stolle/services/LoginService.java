package dev.stolle.services;


import dev.stolle.daos.UserDao;
import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.User;

public class LoginService {
	
	private UserDao loginDao = new UserDaoImpl();
	
	public User getUser(String userName, String password) {
		return loginDao.getUser(userName, password);
	}

}
