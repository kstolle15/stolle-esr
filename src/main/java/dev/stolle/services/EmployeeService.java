package dev.stolle.services;

import java.sql.SQLException;

import dev.stolle.daos.RequestDao;
import dev.stolle.daos.RequestDaoImpl;
import dev.stolle.daos.UserDao;
import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.Request;
import dev.stolle.models.User;

public class EmployeeService {
	private RequestDao employeeRequestDao = new RequestDaoImpl();
	private UserDao employeeUserDao = new UserDaoImpl();
	
	public User getUser(int userId) {
		return employeeUserDao.getUser(userId);
	}
	
	public boolean submitRequst(Request request) {
		boolean success = false; 
		try {
			success = employeeRequestDao.submitRequest(request);
		} catch (SQLException ex) {
			System.out.println("Failed to submitt request.");
			ex.printStackTrace();
		}
		
		return success;
	}
}
