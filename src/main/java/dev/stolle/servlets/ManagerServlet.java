package dev.stolle.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.stolle.models.Request;
import dev.stolle.models.RequestStatus;
import dev.stolle.models.User;
import dev.stolle.services.LoginService;
import dev.stolle.services.ManagerService;

public class ManagerServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ManagerService managerService = new ManagerService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Retrieving all employee pending requests for manager.");
		String userIdString = request.getParameter("userId");
		int userId = Integer.parseInt(userIdString);
		User user = managerService.getUser(userId);
		List<Request> requests = managerService.getAllPendingRequests();
		ObjectMapper om = new ObjectMapper();
		String userJson = om.writeValueAsString(user);
		String requestsJson = om.writeValueAsString(requests); 
		String fullJson = userJson.substring(0,userJson.length()-1) + ", \"userRequests\": " + requestsJson + "}";
		PrintWriter pw = response.getWriter();
		pw.write(fullJson);
		pw.close();
		System.out.println("Successfully logged in manager.");
		System.out.println("Successfully retrieved all employyee requests.");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		BufferedReader br = request.getReader();
		String json = "";
		String line = br.readLine();
		while(line != null) {
			json = json + line;
			line=br.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Request req = om.readValue(json, Request.class);
		System.out.println(req);
		if(req.getStatus() == RequestStatus.APPROVED) {
			if(managerService.approveRequest(req)){
				System.out.println("Successfully Approved request.");
			}else {
				System.out.println("Failed to Approve request.");
			}
		} else {
			if(managerService.rejectRequest(req)) {
				System.out.println("Successfully Rejected Request.");
			} else {
				System.out.println("Failed to Reject Request.");
			}
			
		}
	}
}
