package dev.stolle.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.stolle.models.Request;
import dev.stolle.models.User;
import dev.stolle.services.EmployeeService;
import dev.stolle.services.LoginService;

public class EmployeeServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EmployeeService employeeService = new EmployeeService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Getting user information.");
		String userIdString = request.getParameter("userId");
		int userId = Integer.parseInt(userIdString);
		User user = employeeService.getUser(userId);
		ObjectMapper om = new ObjectMapper();
		String userJson = om.writeValueAsString(user);
		PrintWriter pw = response.getWriter();
		pw.write(userJson);
		pw.close();
		System.out.println("Successful log in attempt. Found user: " + user.getUserName());
		System.out.println("Retreiving user information.");
	} 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Submitting a new request.");
		BufferedReader br = request.getReader();
		String json = "";
		String line = br.readLine();
		while(line != null) {
			json = json + line;
			line=br.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		Request req = om.readValue(json, Request.class);
		System.out.println(req);
		if(employeeService.submitRequst(req)) {
			System.out.println("Successfully submitted request.");
		} else {
			System.out.println("Failed to submit request.");
		}
		br.close();
		
	}
}
