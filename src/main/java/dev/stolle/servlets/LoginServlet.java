package dev.stolle.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.stolle.models.User;
import dev.stolle.models.UserType;
import dev.stolle.services.LoginService;

public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LoginService loginService = new LoginService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("POST request for login servlet");
		BufferedReader br = request.getReader();
		String json = "";
		String line = br.readLine();
		while(line != null) {
			json = json + line;
			line=br.readLine();
		}
		System.out.println(json);
		ObjectMapper om = new ObjectMapper();
		User user = om.readValue(json, User.class); 
		String userName = user.getUserName();
		String password = user.getPassword();
		
		if(userName == null) {
			System.out.println("Invalid login attempt. No username provided.");
			response.sendError(401);
		} else if (password == null) {
			System.out.println("Invalid login attempt. No password provided.");
			response.sendError(401);
		} else {
			user = loginService.getUser(userName, password);
			if(user == null) {
				System.out.println("Invalid login attempt. User not found in database.");
				response.sendError(401);
			} else {
				PrintWriter pw = response.getWriter();
				String userJson = om.writeValueAsString(user);
				System.out.println(userJson);
				pw.write(userJson);
				pw.close();
			}
		}
		br.close();
	}
}
